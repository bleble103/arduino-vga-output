//   640x480 60Hz VGA Output - square
//   Arduino UNO R3

//HSYNC pin 2
#define HSYNC_LOW PORTD &= ~(1<<PD2);
#define HSYNC_HIGH PORTD |= (1<<PD2);

//VSYNC pin 3
#define VSYNC_LOW PORTD &= ~(1<<PD3);
#define VSYNC_HIGH PORTD |= (1<<PD3);

//RED   pin 4
#define RED_OFF PORTD &= ~(1<<PD4);
#define RED_ON PORTD |= (1<<PD4);

//LED for debugging
#define LED_ON PORTB |= (1<<PB5);
#define LED_OFF PORTB &= ~(1<<PB5);

//Number of prescaled timer counts to start and stop HSYNC pulse
uint8_t hSyncStart = 56;
uint8_t hSyncStop = 64;

//line currently being drawn - 33 lines of back porch, 480 active lines, 10 front porch, 2 pulse
uint16_t lines = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  RED_OFF;


  //Normal timer0 behavior - nonPWM
  TCCR0A &= ~(1<<WGM00);
  TCCR0A &= ~(1<<WGM01);
  TCCR0B &= ~(1<<WGM02);
  
  //set counter0 prescaler to 8
  TCCR0B &= ~(B00000101);
  TCCR0B |= B00000010;

  //set counter1 prescaler to 1
  TCCR1B &= ~(B00000110);
  TCCR1B |= B00000001;
  
  //Normal timer1 behavior - nonPWM
  TCCR1A &= ~(1<<WGM10);
  TCCR1A &= ~(1<<WGM11);
  TCCR1B &= ~(1<<WGM12);

  //Output compare register A for timer0
  OCR0A = hSyncStart;
  
  //Output compare register B for timer0
  OCR0B = hSyncStop;

  //Enable compare A interrupt
  TIMSK0 |= (1<<OCIE0A);
  
  //Enable compare B interrupt
  TIMSK0 |= (1<<OCIE0B);

}
ISR(TIMER0_COMPA_vect){
  //HSYNC pulse interrupt
  HSYNC_LOW;
  
}

ISR(TIMER0_COMPB_vect){
  //After HSYNC pulse interrupt
  
  //Reset counter timer 0
  TCNT0 = 0;

  //Reset counter timer 1
  TCNT1 = 0;
  
  //hSyncHIGH
  HSYNC_HIGH;

  //lines
  lines++;
  if(lines == 523){
    //VSYNC pulse
    VSYNC_LOW;
  }
  else if (lines == 525){
    //End of VSYNC pulse
    VSYNC_HIGH;

    //reset lines
    lines = 0;
  }

}
void loop() {
  if((TCNT1 > 30 and TCNT1 < 70) or(TCNT1 > 360 and TCNT1 < 400) or (lines>35 and lines < 85) or (lines>465 and lines < 515)){
    //Red square boundary
    RED_ON;
  }
  else{
    RED_OFF;
  }
  // put your main code here, to run repeatedly:
}
